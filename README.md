# xximg

#### 项目介绍
一款用于前端图片调整的jquery插件  
在前端实践中，常常会出会图片的尺寸和设计尺寸不匹配的情况，导致页面差位，或是图片很难看。传统的解决方案要么损失的图片的展示效果，要么使用背景
图片的方式用css3定位。
这里提的供了另一个选择，希望可以帮你解决一些问题

#### 设计思路
插件基于jquery开发，实现思路为：  
 获取目标图片的尺寸，创建等大小的canvas,将原图 伸缩 或 裁切 到目标大小，并重新定位，然后将canvas重新绘到img中。
 
> 纯前端js操作，无需后台支持  
> 原图的src会被base64格式的数据覆盖  
> 原图的src保留在 original_src 属性中  

#### 安装教程

1. 引入js文件  
```<script src="js/xximg.js"></script>```
2. 给需求调整的图片加一个选择器（非必需）,如 ```class="... xximg" ```
3. 在jquery中初始化   
```
 $(function () {
        $('.xximg').xximg();
 })
```
#### 使用说明
[基本用法](http://www.gx1727.com/static/xximg/index.html)  
[进阶用法](http://www.gx1727.com/static/xximg/demo3.html)  

**自定义标签属性**  
插件识别的自定义标签属性以`_xx_`开头的属性  
可用的属性如下：  

* _xx_width： 目标宽度  数值 或 parent 或 parent.parent  
parent: 以父级为准  
parent.parent  以父级的父级为准  
* _xx_height：目标高度  数值  或 parent 或 parent.parent  
parent: 以父级为准  
parent.parent  以父级的父级为准  
* _xx_mod：处理模式 目前支持两种:  
elastic: 伸缩  *图片会伸缩以达到全部显示出来，会留有旁白*  
cutting: 裁切  *图片会伸缩调整到充满全部目标大小，在根据 xx_pos 的位置要求裁切*
* _xx_pos：裁切位置，取值0-1之间。  *只有在 裁切 模式下有效，如：0.5， 按中间位置裁切*
* _xx_autoresize：自动响应resize事件 取值 `on` 是自动响应，其它都没自动响应resize事件

基本img标签如下:
```
<img src="img/1.jpg" _xx_width="200" _xx_height="200" _xx_mod="elastic">

注：原图将伸缩至合适大小，最终获取一张200*200的图片
```

```
<img src="img/2.jpg"  _xx_width="200" _xx_height="200" _xx_mod="cutting" xx_pos='0'>

注：原图将被裁切至合适大小，裁切位置为长边的开始位置，最终获取一张200*200的图片
这里说明的是，如果原图为竖图，xx_pos=0即为从上往下裁切。如果原图为横图，xx_pos=0即为从左往右裁切
```

```
<img src="img/2.jpg"  style="width: 100%;" _xx_height="200" _xx_mod="cutting" xx_pos='0.5'>

注：这里没有指定_xx_width，则xximg会计算img本身宽作为目标宽度
```

**js调用参数**  
基本用法如下即可：
```
$('.xximg').xximg();
```
初始时可设置参数如下：  
* backgroundColor： 旁白颜色，在有旁边时会体现，如：  
白色：#FFFFFF  
透明：rgba(255, 255, 255, 0)  
* start： 处理开始时的样式，如：{opacity: 0}，即处理开始时，将图片置为透明  
* end： 处理结束时的样式，如：{opacity: 1}，即处理结束后，将图片置为不透明  
* speed：处理结束时，动画时间（毫秒）  
* mod：处理模式，参数如 _xx_mod, 全局设置，当显式设置了xx_mod，以标签性属为准  
* pos： 裁切位置 参数如 _xx_pos, 全局设置，当显式设置了xx_pos，以标签性属为准  
* autoresize： 自动响应resize事件  参数如 _xx_autoresize, 全局设置，当显式设置了xx_autoresize，以标签性属为准  
* get_width：获到宽度的方法，高级使用接口，可自定义获到宽度的方法  
* get_height：获到高度的方法，高级使用接口，可自定义获到高度的方法

例:
```
$('.xximg').xximg({backgroundColor: 'rgba(255, 255, 255, 0) '});
设置透明背景
```

#### 版本功能
1. v1.0 前端调整图片

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)